`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    11:46:46 01/01/2019
// Design Name:
// Module Name:    expand_key
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module expand_key(
    input [7:0] key_in [15:0],
	 input [7:0] sbox [255:0],
	 output [31:0] key_out [23:0]
    );

	reg [7:0] roundKey [5:0] = {
										8'b00000000,
										8'b00000001,
										8'b00000010,
										8'b00000100,
										8'b00001000,
										8'b00010000
										};

	// init key_out[0] - key_out[3]
	integer i;
	reg [31:0] temp;
	
	always @(key_in) begin
		for (i = 0; i < 4; i = i + 1) begin
			key_out[i] = {key_in[i * 4 + 0], key_in[i * 4 + 1], key_in[i * 4 + 2], key_in[i * 4 + 3]};
		end

		// key_expand core !
		for (i = 4; i < 24; i = i + 1) begin
			if (i % 4 == 0) begin
				// rotate left
				temp = {key_out[i - 1][23:0], key_out[i - 1][31:24]};

				// subBytes
				temp[7:0] = sbox[temp[7:4] * 16 + temp[3:0]];
				temp[15:8] = sbox[temp[15:12] * 16 + temp[11:8]];
				temp[23:16] = sbox[temp[23:20] * 16 + temp[19:16]];
				temp[31:24] = sbox[temp[31:28] * 16 + temp[27:24]];

				// xor with roundKey
				temp = temp ^ {roundKey[i / 4], 8'b00000000, 8'b00000000, 8'b00000000};

				key_out[i] = key_out[i - 4] ^ temp;

			end
			else begin
				key_out[i] = key_out[i - 1] ^ key_out[i - 4];
			end
		end
	end



endmodule
