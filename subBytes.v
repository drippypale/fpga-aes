`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    13:10:26 01/01/2019
// Design Name:
// Module Name:    subBytes
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module subBytes(
    input [7:0] state_in [15:0],
	 input [7:0] sbox [255:0],
    output [7:0] state_out [15:0]
    );

	integer i;
	always @(state_in) begin
		for (i = 0; i < 16; i = i + 1) begin
			state_out[i] = sbox[state_in[i][7:4] * 16 + state_in[i][3:0]];
		end
	end

endmodule
