`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:    11:42:47 01/01/2019
// Design Name:
// Module Name:    AES_Encryptor
// Project Name:
// Target Devices:
// Tool versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////
module AES_Encryptor(
    input [127:0] key_in_raw,
    input [127:0] data_in,
	input reg [7:0] sbox [255:0],
    output [127:0] data_out
    );

	integer i;
	integer j;
	reg [7:0] init_state [15:0];
    reg [7:0] key_in [15:0];
    wire [7:0] add_round_key_output [15:0];
	wire [7:0] subBytes_output [15:0];
	wire [7:0] shift_row_output [15:0];

    wire [31:0] expanded_key [23:0];

	always @(data_in or key_in_raw) begin
        // convert key_in_raw to 1 byte size vector
        for (i = 0; i < 16; i = i + 1) begin
            key_in[i] = key_in_raw[i * 8 + 7 : i * 8];
        end

       // expand key
       expand_key ek_ins(key_in, sbox, expanded_key);

       // init first state
	   for (i = 0; i < 16; i = i + 1) begin
            init_state[i] = data_in[i * 8 + 7 : i * 8];
        end

		// first round
		add_round_key ark_ins({expanded_key[0], expanded_key[1], expanded_key[2], expanded_key[3]}, init_state, add_round_key_output);

		// round 1:5
		for (i = 1; i < 5; i = i + 1) begin
			subBytes sb_ins(add_round_key_output, sbox, subBytes_output);
			shift_row sr_ins(subBytes_output, shift_row_output);
			add_round_key ark_ins_1({expanded_key[i * 4 + 0], expanded_key[i * 4 + 1], expanded_key[i * 4 + 2], expanded_key[i * 4 + 3]}, shift_row_output, add_round_key_output);
		end

        for (i = 0; i < 16; i = i + 1) begin
             data_out[i * 8 + 7 : i * 8] = add_round_key_output[i];
        end

	end

endmodule
